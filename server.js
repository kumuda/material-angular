var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Mood = require('./app/models/mood');
var q = require('q');
// var Mood = ("./app/models/mood");
var app = express();

app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

app.use(allowCrossDomain);

app.post('/upload-mood', function(req, resp) {
  mongoose.Promise = global.Promise;	
  mongoose.connect('mongodb://localhost/instamood');
  var newMood = new Mood({
  	mood: req.body.content,
  	location: req.body.location,
  	created_at: new Date(),
  	updated_at: new Date()
  });

  console.log(newMood);

  newMood.save(function(err){
  	if(err){
  		console.log('Error occured');
  		 resp.send({'error' : err}); 
  		throw err;
  	}
  	else{
  		resp.send(newMood);
  		console.log("saved");
  	}
  });

});


app.listen(8089, function () {
  console.log('Example app listening on port 8089!');
});