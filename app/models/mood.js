var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var moodSchema = new Schema({
   mood: String,
  location: String,
  created_at: Date,
  updated_at: Date
});

// the schema is useless so far
// we need to create a model using it
var Mood = mongoose.model('Mood', moodSchema);

// make this available to our users in our Node applications
module.exports = Mood;