var app = angular.module('InstamoodApp');

app.config(['cloudinaryProvider', function (cloudinaryProvider) {
  cloudinaryProvider
      .set("cloud_name", "mysampleapp")
      .set("upload_preset", "ang_material");
}]);