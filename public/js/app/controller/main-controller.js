var app = angular.module('InstamoodApp');

app.controller('MainController', function($scope,$timeout, $mdSidenav, $log, $http, $q,$rootScope, $routeParams, $location,$mdMedia,$mdDialog, Upload, cloudinary){
     $scope.images = [
                                { "url" : "http://res.cloudinary.com/mysampleapp/image/upload/v1468924789/sample_app_images/1010404_10201913666297744_649074560_n_1.jpg",         "content" : "Info About the Image" },
                                { "url" : "http://res.cloudinary.com/mysampleapp/image/upload/v1468924871/sample_app_images/10263996_10153990180000401_606446898928470806_o.jpg",     "content" : "Info About the Image" },
                                { "url" : "http://res.cloudinary.com/mysampleapp/image/upload/v1468924868/sample_app_images/860788_545786635455511_1262199090_o.jpg",   "content" : "Info About the Image" },
                                { "url" : "http://res.cloudinary.com/mysampleapp/image/upload/v1468924789/sample_app_images/1010404_10201913666297744_649074560_n_1.jpg", "content" : "Info About the Image" },
                                { "url" : "http://res.cloudinary.com/mysampleapp/image/upload/v1468924991/sample_app_images/920326_10200345424932690_2141775997_o.jpg", "content":"Info About the Image"},
                                { "url" : "http://res.cloudinary.com/mysampleapp/image/upload/v1468924597/sample.jpg", "content": "Info About the Image."},
                                { "url" : "http://res.cloudinary.com/mysampleapp/image/upload/v1468925265/sample_app_images/DSCN0449.jpg", "content" : "Info About the Image"},
                                { "url" : "http://res.cloudinary.com/mysampleapp/image/upload/v1468925248/sample_app_images/DSCN0483.jpg", "content" : "Info About the Image"},
                                { "url" : "http://res.cloudinary.com/mysampleapp/image/upload/v1468925196/sample_app_images/DSCN0472.jpg", "content" : "Info About the Image"},
                             ];         

  $scope.toggleMenu = function() {
    $mdSidenav('left').toggle();
  } 

  $scope.openUploadModal = function(ev) {
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
    $mdDialog.show({
      templateUrl: 'templates/upload.tmpl.html',
      controller: 'UploadController',
      // parent: angular.element(document.body),
      // targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    })
  }                     
      
  });
